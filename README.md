# Overview

Environment tests.


# How to install and config JMeter:
on mac: $ brew install jmeter --with-plugins

In order to increase jmeter performance:  
(http://stackoverflow.com/questions/29789800/i-m-getting-java-net-socketexception-connection-reset-exception-while-running-a)
  
1) Change "Implementation" of all your HTTP Request samplers to HttpClient4. The easiest way of doing it is using HTTP Request Defaults configuration element.  
2) Add the following properties in user.properties file which located under /bin folder of your JMeter installation:  
    (on mac: /usr/local/Cellar/jmeter/3.0/libexec/bin)  
    httpclient4.retrycount=1  
    hc.parameters.file=hc.parameters  
3) Add the next line to hc.parameters file (same location, JMeter's /bin folder)  
    http.connection.stalecheck$Boolean=true  
4) Restart JMeter.  
  
Config Graphs Generator Listener:
(https://jmeter-plugins.org/wiki/GraphsGeneratorListener/)

All you need just to add the following property to user.properties

jmeter.save.saveservice.autoflush=true

# How to run JMeter:

1) $ export PATH=$PATH:/path/to/dir  
(on mac: $ export PATH=$PATH:/usr/local/Cellar/jmeter/3.0/libexec/bin)  
2) go to test folder  
3) run jmeter  
    GUI mode: jmeter  
    Command line mode: jmeter -n -t /path/to/dir/dummy.jmx

Run performance tests only in command line mode!

